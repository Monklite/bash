#!/bin/bash

ConfDirectory=${XDG_CONFIG_HOME:-"$HOME/.config"}/journal

# Check any arguments
case $1 in
	-help)
		echo "Run the script as is or use the -uninstall flag to remove the script"
		exit 1
		;;
	-uninstall)
		sudo rm /usr/bin/journal && rm -r $ConfDirectory
		exit 1
		;;
esac

# Move file to /usr/bin and create conf directory and file
sudo cp ./journal /usr/bin
sudo chmod +x /usr/bin/journal
mkdir $ConfDirectory
touch $ConfDirectory/config

# Read journal location
echo "Please type the full path to your journal file"
read location

# Check validity of location
if [ -f $location ]; then
	echo "Location successfully added to $ConfDirectory/config"
	echo "$location" >> $ConfDirectory/config
	gpg -c --batch --yes $location
    echo "Journal script successfully installed. Delete the unencrypted journal if you want to."
else
	echo "Something happened. Make sure the full path to your journal exists (create it first, then re-run)"
fi
